import { Auth } from 'aws-amplify'

export default async function ({ store }) {
  let user = null
  try {
    user = await Auth.currentUserPoolUser()
  } catch (err) {
    store.$router.push('/auth/login')
    return
  }
  store.commit('auth/set', { username: user.username, ...user.attributes })
}
