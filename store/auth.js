
import { Auth } from 'aws-amplify'

export const state = () => ({
  user: null
})

export const mutations = {
  set (state, user) {
    state.user = user
  },
  unset (state) {
    state.user = null
  }
}

export const actions = {
  async logout ({ commit }) {
    try {
      await Auth.signOut()
      commit('unset')
      this.app.router.push('/auth/login')
    } catch (err) {
      console.log(err)
    }
  }
}
